#include "Arduino.h"
void check_RF24_Network(){

  //-----------------------------------nRF24L01--------------------------------------
  // Pump the network regularly
  // 
  network.update();


  // Is there anything ready for us?
  // Есть ли что-нибудь, готовое к нам?
  while ( network.available() )
  {

    // If so, take a look at it 
    // Если это так, посмотрите на него
    RF24NetworkHeader header;
    network.peek(header);
#ifdef DEDUG
#ifdef Time
    printf_P(PSTR("%d:%d:%d %d.%d.%d : APP Received #%u type %c from 0%o\n\r"),duino_client.hour_struct,duino_client.minute_struct,
    duino_client.second_struct,duino_client.day_struct,duino_client.month_struct,duino_client.year_struct,
    header.id,header.type,header.from_node);
#endif
    printf_P(PSTR("%lu: APP Received #%u type %c from 0%o\n\r"),millis(),header.id,header.type,header.from_node);
#endif
    // Dispatch the message to the correct handler.
    // Отправить сообщение на правильный обработчик.
    switch (header.type)
    {
    case 'M':
//      LED_ON;    
      handle_M(header);
//      LED_OFF;
      break;
    default:
#ifdef DEDUG
      printf_P(PSTR("*** WARNING *** Unknown message type %c\n\r"),header.type);
#endif
      network.read(header,0,0);
      break;
    };
  }
  //-----------------------------------nRF24L01--------------------------------------

}









