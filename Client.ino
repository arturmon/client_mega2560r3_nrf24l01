
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <RF24Network.h>
#include "printf.h"
#include <OneWire.h>

#include "A05_Send_and_Handle_NF24.h"
#include "system.h"
System sys;

#include <Wire.h> //BH1750 IIC Mode
#include <math.h> //BH1750



int BH1750address = 0x23; //setting i2c address //BH1750
byte buff[2]; //BH1750
#include <longtime.h>

#define DEDUG;
#define Time;
#define BUDILNIK;

#ifdef Time
#include <Time.h> 
#include <TimeAlarms.h>
time_t prevDisplay = 0; // when the digital clock was displayed
#endif

#define RELE_1 8 //Arduino 8 Порт B0
#define RELE_2 7 //Arduino 7 порт D7
#define RELE_3 6 //Arduino 6 порт D6
#define RELE_4 5 // Arduino 5 порт D5

#define ON 0
#define OFF 1

//#define HIGH 0x1
//#define LOW  0x0

//#define INPUT 0x0
//#define OUTPUT 0x1

//Analog_Read
#define FASTADC 1

// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

int inputPin_Pir = 4;               // choose the input pin (for PIR sensor)
//int pirState = LOW;             // we start, assuming no motion detected
int val_pir = 0;                    // variable for reading the pin status

//int Temp_Error_Message = 0;

//
//  OneWire
//
OneWire ds(3);      //Создаем датчик DS18B20 на 3 пине

/*
const int LEDpin = 13;

#define LED_ON digitalWrite(LEDpin, HIGH);
#define LED_OFF digitalWrite(LEDpin, LOW);
*/
//
// Hardware configuration
//
//-----------------------------------Устанавливаем таймеры-----------------------------------

CLongTimer tmr1(20);//через какое время будет проиведен следующий опрос elapsed1
CLongTimer tmr2(1000);//через какое время будет проиведен следующий опрос elapsed2
CLongTimer tmr3(15000);//через какое время будет проиведен следующий опрос elapsed3


//-----------------------------------Уствнвыливаем таймеры-----------------------------------

long Default_interval = 60;           // interval 6sec

//создаём структуру для передачи значений---------------------------------------------------------
struct payload_t
{
	byte type;          //<< Тип датчика
	byte number;        //<< Номер датчика
	int value;                //<< Значения датчика
}
F_tt;


typedef struct{
#ifdef Time
  byte second_struct;
  byte minute_struct;
  byte hour_struct;
  byte day_struct;
  byte month_struct;
  int year_struct;	
#endif  
	
	float temperature_Sensor;// передаём температуру )
	
	int Analog_sensor_A0;//еще датчик
	int Analog_sensor_A1;//еще датчик
	int Analog_sensor_A2;//еще датчик
	int Analog_sensor_A3;//еще датчик
	
	int Digital_sensor_D3;
	int Digital_sensor_D4;
	boolean Rele_1;
	boolean Rele_2;
	boolean Rele_3;
	boolean Rele_4;
	
	uint16_t val_BH1750;
	boolean pirState;
	
  int temp_Lost_packets;
  float Lost_packets;            //Lost_packets          //ID:240
  int Send_packets;
  unsigned int Sending_interval;             //Send_interval         //ID:203   //max_Value:65535 (секунд)
	
	int Free_RAM;
	char* Uptime;
}
B_t;

B_t duino_client;

//int count;    //переменная для счётчика циклов
//создаём структуру для передачи значений---------------------------------------------------------

//-----------------------------------nRF24L01--------------------------------------

// nRF24L01(+) radio using the Getting Started board
RF24 radio(9,10); //rf_ce,rf_csn
RF24Network network(radio);

/*
Node 00 is the base node.
Nodes 01-05 are nodes whose parent is the base.
Node 021 is the second child of node 01.
Node 0321 is the third child of node 021, an so on.
The largest node address is 05555, so 3,125 nodes are allowed on a single channel.
*/

// Адрес нашего узла
const uint16_t this_node = 012;

// Адрес приемника
const uint16_t to = 00;
