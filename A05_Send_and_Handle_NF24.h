

#ifndef __A05_SEND_AND_HANDLE_RF24_H__
#define __A05_SEND_AND_HANDLE_RF24_H__

extern bool send_M(uint16_t to,unsigned int type,unsigned int number,float value);
extern bool send_N(uint16_t to);
extern bool send_I(uint16_t to,unsigned int type,unsigned int number,float value);
extern void handle_M(RF24NetworkHeader& header);
extern void handle_N(RF24NetworkHeader& header);
extern void handle_I(RF24NetworkHeader& header);
extern void add_node(uint16_t node);


#endif // __A05_SEND_AND_HANDLE_RF24_H__
