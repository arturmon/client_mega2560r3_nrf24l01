/* 
	Editor: http://www.visualmicro.com
	        arduino debugger, visual micro +, free forum and wiki
	
	Hardware: Arduino Uno, Platform=avr, Package=arduino
*/

#define __AVR_ATmega328P__
#define ARDUINO 101
#define ARDUINO_MAIN
#define F_CPU 16000000L
#define __AVR__
#define __cplusplus
extern "C" void __cxa_pure_virtual() {;}

void check_RF24_Network();
bool send_M(uint16_t to);
void handle_M(RF24NetworkHeader& header);
void Send_base_to_mesage_on_node(byte number, byte type, float value);
void elapsed1();
void elapsed2();
void elapsed3();
void  Update_Variable();
void Update_System();
void Send_Value_To_Mega();
//
//
int BH1750_Read(int address);
void BH1750_Init(int address);
float Get_18B20_Data();
void read_Digital_Sensors();
void read_BH1750();
void read_PIR();
void Reboot();

#include "D:\util\arduino-1.5.2\hardware\arduino\avr\variants\standard\pins_arduino.h" 
#include "D:\util\arduino-1.5.2\hardware\arduino\avr\cores\arduino\arduino.h"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\Client.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\A03_Check_RF24.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\A05_Send_and_Handle_NF24.h"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\A05_Send_and_Handle_NF24.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\A15_Utils.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\B01_Void_Setup.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\B02_Void_Loop.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\B03_BH1750_Read.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\B04_BH1750_Init.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\B05_Get_18B20_Data.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\B10_void_read_Sensors.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\B99_void_reboot.ino"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\System.cpp"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\System.h"
#include "\\nas-local\Mirror\arturmon\Arduino_Scetch\local_arduino\Arturmon\Клиенты\Client\printf.h"
