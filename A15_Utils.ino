
void elapsed1()
{

	#ifdef DEDUG
	printf_P(PSTR("-----------------TIMER-1(tmr1)----------------- "));
	printf_P(PSTR("%lu"),millis()/1000);
	printf_P(PSTR("c \n"));
	#endif
	check_RF24_Network();
	read_PIR();

}

void elapsed2()
{

	#ifdef DEDUG
	printf_P(PSTR("-----------------TIMER-2(tmr2)----------------- "));
	printf_P(PSTR("%lu"),millis()/1000);
	printf_P(PSTR("c \n"));
	#endif
	
	Update_Variable();


}

void elapsed3()
{

	#ifdef DEDUG
	printf_P(PSTR("-----------------TIMER-4(tmr4)----------------- "));
	printf_P(PSTR("%lu"),millis()/1000);
	printf_P(PSTR("c \n"));
	#endif
	Update_System();
	read_Digital_Sensors();
	Send_Value_To_Mega();
}



void  Update_Variable(){

	//---------------------------------read_sensors--------------------------
	


	duino_client.Analog_sensor_A0 = analogRead(A0)/102.3;
	duino_client.Analog_sensor_A1 = analogRead(A1)/102.3;
	duino_client.Analog_sensor_A2 = analogRead(A2);
	duino_client.Analog_sensor_A3 = analogRead(A3);
	duino_client.Digital_sensor_D3 = digitalRead(3);
	//duino_client.Rele_1 = digitalRead(RELE_1);
	//duino_client.Rele_2 = digitalRead(RELE_2);
	//duino_client.Rele_3 = digitalRead(RELE_3);
	//duino_client.Rele_4 = digitalRead(RELE_4);

	// Заполнение структуры данными
	

	if (duino_client.Rele_1==1) digitalWrite(RELE_1, ON);
	if (duino_client.Rele_1==0) digitalWrite(RELE_1, OFF);
	if (duino_client.Rele_2==1) digitalWrite(RELE_2, ON);
	if (duino_client.Rele_2==0) digitalWrite(RELE_2, OFF);
	if (duino_client.Rele_3==1) digitalWrite(RELE_3, ON);
	if (duino_client.Rele_3==0) digitalWrite(RELE_3, OFF);
	if (duino_client.Rele_4==1) digitalWrite(RELE_4, ON);
	if (duino_client.Rele_4==0) digitalWrite(RELE_4, OFF);
}

void Update_System(){
	duino_client.Free_RAM=sys.ramFree();
	duino_client.Uptime=sys.uptime();
  float temp = float(duino_client.Send_packets)/100;
  duino_client.Lost_packets= (float(duino_client.temp_Lost_packets)/temp);
	Serial.println(duino_client.Send_packets);
	Serial.println(duino_client.temp_Lost_packets);
	Serial.println(duino_client.Lost_packets);
}




void Send_Value_To_Mega(){
	
	//Всегда на MEga2560 R3
	uint16_t to = 00;


	//bool ok;
	//отправляем температуру
	Send_base_to_mesage_on_node(1,10,duino_client.val_BH1750,0);

	//отправляем состояние реле
	Send_base_to_mesage_on_node(2,2,duino_client.temperature_Sensor,0);



	Send_base_to_mesage_on_node(3,4,duino_client.Analog_sensor_A0,0);
	//------------------------------------------------
	Send_base_to_mesage_on_node(4,4,duino_client.Analog_sensor_A1,0);
	//------------------------------------------------
	Send_base_to_mesage_on_node(5,4,duino_client.Analog_sensor_A2,0);
	//------------------------------------------------
	Send_base_to_mesage_on_node(6,4,duino_client.Analog_sensor_A3,0);


	Send_base_to_mesage_on_node(7,5,duino_client.Digital_sensor_D3,0);
	//------------------------------------------------
	Send_base_to_mesage_on_node(8,5,duino_client.Digital_sensor_D4,0);

	//------------------------------------------------
	Send_base_to_mesage_on_node(9,19,duino_client.Rele_1,0);
	//------------------------------------------------
	Send_base_to_mesage_on_node(10,19,duino_client.Rele_2,0);
	//------------------------------------------------
	Send_base_to_mesage_on_node(11,19,duino_client.Rele_3,0);
	//------------------------------------------------
	Send_base_to_mesage_on_node(12,19,duino_client.Rele_4,0);


	//------------------------------------------------
	//Send_base_to_mesage_on_node(13,13,duino_client.pirState);

	Send_base_to_mesage_on_node(14,200,duino_client.Free_RAM,0);
	Send_base_to_mesage_on_node(15,240,duino_client.Lost_packets,0);
  Send_base_to_mesage_on_node(16,203,duino_client.Sending_interval,0);


	
}


void MorningAlarmBudilnik_1(){
//  RGB_Controller.type = 0;
//  RGB_Controller.Rele_1 = true;

}
void MorningAlarmBudilnik_2(){
//  RGB_Controller.Rele_1 = false;
}

void MorningAlarm(){
  printf_P(PSTR("Alarm: - пора обновлять время"));
  Send_base_to_mesage_on_node(0,0,0,1);
}

void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(" ");
  Serial.print(month());
  Serial.print(" ");
  Serial.print(year()); 
  Serial.println(); 
}

void printDigits(int digits)
{
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}








