void read_Digital_Sensors(){
read_BH1750();
read_PIR();
duino_client.temperature_Sensor = Get_18B20_Data(); //DS18B20
}

void read_BH1750(){
//******************************Read   BH1750********************************//

	  int i;
	  BH1750_Init(BH1750address);
	  delay(200);
	 
	  if(2==BH1750_Read(BH1750address))
	  {
	    duino_client.val_BH1750=((buff[0]<<8)|buff[1])/1.2;
//	    Serial.print(val,DEC);    
//	    Serial.println("[lx]");
	  }
	  
//******************************Read   BH1750********************************//  
}

void read_PIR(){
//******************************Read   PIR***********************************//

  val_pir = digitalRead(inputPin_Pir);  // read input value
  if (val_pir == HIGH) {            // check if the input is HIGH
//    digitalWrite(led, HIGH);  // turn LED ON
    if (duino_client.pirState == LOW) {
      // we have just turned on
      Serial.println("Motion detected!");
      // We only want to print on the output change, not state
      duino_client.pirState = HIGH;
	  Send_base_to_mesage_on_node(13,13,duino_client.pirState,0);
    }
  } else {
//    digitalWrite(led, LOW); // turn LED OFF
    if (duino_client.pirState == HIGH){
      // we have just turned of
      Serial.println("Motion ended!");
      // We only want to print on the output change, not state
      duino_client.pirState = LOW;
	  Send_base_to_mesage_on_node(13,13,duino_client.pirState,0);
    }
  }
//******************************Read   PIR***********************************//  
}
