void setup() {
//	pinMode(LEDpin, OUTPUT);
  duino_client.Sending_interval	= Default_interval;	
	Serial.begin(57600);
	Serial.println("Start");
	//Analog_Read
	#if FASTADC
	// set prescale to 16
	sbi(ADCSRA,ADPS2) ;
	cbi(ADCSRA,ADPS1) ;
	cbi(ADCSRA,ADPS0) ;
	#endif

	//-----------------------------------nRF24L01-----------------------------------------------
	SPI.begin();
	radio.begin();
	network.begin(/*channel*/ 100, /*node address*/ this_node );

	//-----------------------------------nRF24L01-----------------------------------------------

	delay(100);

	Wire.begin(); //BH1750 and BMP085
	delay(1000);


	pinMode(RELE_1, OUTPUT);
	pinMode(RELE_2, OUTPUT);
	pinMode(RELE_3, OUTPUT);
	pinMode(RELE_4, OUTPUT);

	
	digitalWrite(RELE_1, OFF);
	digitalWrite(RELE_2, OFF);
	digitalWrite(RELE_3, OFF);
	digitalWrite(RELE_4, OFF);

	pinMode(3, INPUT);
	pinMode(inputPin_Pir, INPUT);     // declare sensor as input

#ifdef Time
  Send_base_to_mesage_on_node(0,0,0,1);
  //������������� ��������� �� ������ ���� ��� ���������� ������� � ����

#endif
	Update_System();
	read_Digital_Sensors();
	Update_Variable();
	delay(100);


	Send_Value_To_Mega();

	tmr1.event(&elapsed1);
	tmr2.event(&elapsed2);
	tmr3.event(&elapsed3);

	tmr1.start();
	tmr2.start();
	tmr3.start();



}
