#include "Arduino.h"


bool send_M(uint16_t to)
{

	RF24NetworkHeader header(/*to node*/ to, /*type*/ 'M' /*Time*/);
	#ifdef DEDUG
	printf_P(PSTR("---------------------------------\n\r"));
	printf_P(PSTR("%lu: APP +++++ type %d number %d value %d to 0%d...\n\r"),millis(),F_tt.type,F_tt.number,F_tt.value,to);
	#endif
	return network.write(header,&F_tt,sizeof(F_tt));
}

bool send_T(uint16_t to)
{

  RF24NetworkHeader header(/*to node*/ to, /*type*/ 'T' /*Time*/);
#ifdef DEDUG
  printf_P(PSTR("---------------------------------\n\r"));
  printf_P(PSTR("%lu: APP +++++ type %d number %d value %d to 0%d...\n\r"),millis(),F_tt.type,F_tt.number,F_tt.value,to);
#endif
  return network.write(header,&F_tt,sizeof(F_tt));
}



void handle_M(RF24NetworkHeader& header)
{
	payload_t payload;
	network.read(header,&payload,sizeof(payload));
	printf_P(PSTR("---------------------------------\n\r"));
#ifdef Time
  printf_P(PSTR("%d:%d:%d %d.%d.%d : APP Received nodes 0%o type ID %d number %d value %d\n\r"),duino_client.hour_struct,duino_client.minute_struct,
  duino_client.second_struct,duino_client.day_struct,duino_client.month_struct,duino_client.year_struct,
  header.from_node,payload.type,payload.number,payload.value);
#endif 
	printf_P(PSTR("%lu: APP Received nodes 0%o type ID %d number %d value %d\n\r"),millis(),header.from_node,payload.type,payload.number,payload.value);
	if ( header.from_node != this_node || header.from_node > 00 )
	//    add_node(header.from_node);

	if (payload.type == 19 && payload.number == 9) duino_client.Rele_1 = payload.value; //проверяем верность ID и устанавливаем значение в структуре,
	if (payload.type == 19 && payload.number == 10) duino_client.Rele_2 = payload.value; //проверяем верность ID и устанавливаем значение в структуре,
	if (payload.type == 19 && payload.number == 11) duino_client.Rele_3 = payload.value; //проверяем верность ID и устанавливаем значение в структуре,
	if (payload.type == 19 && payload.number == 12) duino_client.Rele_4 = payload.value; //проверяем верность ID и устанавливаем значение в структуре,
#ifdef Time
  if (payload.type == 210) duino_client.second_struct = payload.value;
  if (payload.type == 211) duino_client.minute_struct = payload.value;
  if (payload.type == 212) duino_client.hour_struct = payload.value;
  if (payload.type == 213) duino_client.day_struct = payload.value;
  if (payload.type == 214) duino_client.month_struct = payload.value;
  if (payload.type == 215) duino_client.year_struct = payload.value;
  //устанавливаем время
  if (payload.type == 210 || 211 || 212 || 213 || 214 || 215)
  {
    // часы,минуты,секунды,число,месяц,год
    setTime(duino_client.hour_struct,duino_client.minute_struct,duino_client.second_struct,
    duino_client.day_struct,duino_client.month_struct,duino_client.year_struct);
  }
#endif
	//после обновления переменных значения встурпят в силу
	Update_Variable();

}

void Send_base_to_mesage_on_node(byte number, byte type, float value,byte type_packets)
{
	/*
	byte type;          //<< Тип датчика
	byte number;        //<< Номер датчика
	int value;  F_tt
	*/
  uint16_t to = 00;


  // провека ID на значение float
  bool sensor_type_ok = type == 2 || type == 240 || type == 31 || type == 32 || type == 33;
  if (sensor_type_ok)
  {
    F_tt.value=value*100;
  }
  else if (type == 203) //проверяем случаем не unsigned int нужно передать
  {
    if (value >=0) F_tt.value = value - 32768; // проверяем если значение больше 0 то вычетаем 32768 так мы добиваемся возможности передавать значения до 65535
  }
  else F_tt.value=value;

  bool ok;

  F_tt.number = number;
  F_tt.type = type;

	if ( this_node > 00 || to == 00 )
  {
    if (type_packets == 0){
      //LED_ON;
      printf_P(PSTR("Send packet \"M\"\n\r"));
      ok = send_M(to);
      //LED_OFF;
    }
    else if (type_packets == 1) {
      //LED_ON;
      printf_P(PSTR("Send packet \"T\"\n\r"));
      ok = send_T(to);
      //LED_OFF;			
    }  
  }

	else
	{
		//LED_ON;
		//ok = send_N(to);
		// LED_OFF;
	}


	if (ok)
	{
		#ifdef DEDUG
		if (duino_client.Send_packets < 32767){
			duino_client.Send_packets=duino_client.Send_packets++;
		}

		else{
			duino_client.Send_packets=0;
			duino_client.temp_Lost_packets=0;
		}
		printf_P(PSTR("%lu: APP Sending ok\n\r"),millis());
		#endif
	}
	else
	{
		if (duino_client.temp_Lost_packets < 32767){
			duino_client.temp_Lost_packets=duino_client.temp_Lost_packets++;
		}
		else{
			duino_client.Send_packets=0;
			duino_client.temp_Lost_packets=0;
		}
		#ifdef DEDUG
		printf_P(PSTR("%lu: APP Sending failed\n\r"),millis());
		#endif
	}

}


